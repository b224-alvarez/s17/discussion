// alert("Hello World!");
// Functions
// Functions in JS are lines/blocks of codes that tell your device/application to perform a certain task when called/invoked.
// Functions are mostly created to create complicated tasks to run several lines of code in succession.
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function.

// Function Declaration
// (Function statements) - defines a function with the specified parameters.

/* 
            Syntax:
                function functionName() {
                    code block (statement);
                };
        */
// Function Keyword - used to define a JS function.
// functionName - Functions are named to be a able to use later in the program/code.
// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed is found.

function printName() {
  console.log("My name is Levin Alvarez");
}

// Semicolons are used to separate executable JavaScript statments
printName();
printName();

// Function Invocation
// The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is only excuted when the function is invoked or called.

// This is how we invoke/call the function we declared.
printName();

// declaredFunction(); - results in an error, much like variables, we cannot invoke a function we have not yet defined.

// Function declaration vs. Function Expression

// Function Declarations

// A function can be created through function declaration by using the function keyword and adding a function name.

// Declared functions are not executed immediately.

declaredFunction(); //declared functions can be hoisted. As long as the funciton has been defined.

// Note: Hoisting in JavaScript's behavior for certain variables and function to run or use them before their declaration.

function declaredFunction() {
  console.log("Hello World from delaredFunction()");
}

declaredFunction();

// Function Expression
// A function can be also stored in a variable. This is called function expression.

// A function expression is an anonymous function asigned to a variableFunction

// Anonymous function - a function without a name.

// variableFunction();
/* 
    result to an error - function expression, being stored in a let/const variable, cannot be hoisted.
*/

let variableFunction = function () {
  console.log("Hello Again!");
};

variableFunction();

// We can also create a funciton expression of a named function.
// However, to invoke the function expression, we invoke it by it's variable name and not by it's function name.

let funcExpression = function funcName() {
  console.log("Hello from the other side.");
};

// funcName();
funcExpression();

// You can reassign declared functions and function expressions to a new anonymous functions.
declaredFunction();

declaredFunction = function () {
  console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function () {
  console.log("updated funcExpression");
};

funcExpression();

// However, we cannot re-assign a function expression initialized with const keyword.

const constantFunc = function () {
  console.log("This is initialized with const");
};

constantFunc();

/* constantFunc = function () {
  console.log("Cannot be reassigned!");
};

constantFunc(); */

// Function Scoping
/* 
    Scope is the accessibility (visibility) of variables within our program.

    JavaScript Variables has 3 types of scope.
        1. local/block scope
        2. global scope
        3. function scope
*/

// Block Scope
{
  let localVar = "Demi Lovato";

  console.log(localVar);
}

// console.log(localVar); //going to result in an error

// Global Scope

let globalVar = "Mr. Worldwide";

console.log(globalVar);

// Function Scope

/* 
    JavaScript has a function scope: Each function creates a new scope. Variables defined inside a function are not accessible (visible) from outside that function.
*/

function showNames() {
  var functionVar = "Joe";
  const functionConst = "John";
  let functionLet = "Jane";

  console.log(functionVar);
  console.log(functionConst);
  console.log(functionLet);
  console.log(globalVar);
}

showNames();

//All these will result in an error
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

/* 
    The variables functionVar, functionConst and functionLet are function scoped and cannot be accessed outside of the function they were declared in.
*/

// Nested Functions

// You can create another function inside a function. This is called a nested function. This nested funciton, being inside the parent function will have access to the variable as they are within the same scope or code block.

function myNewFunction() {
  let name = "Jessica";

  function nestedFunction() {
    let nestedName = "Joseph";
    console.log(name);
  }
  //   console.log(nestedName); This results to an error.
  nestedFunction();
}
myNewFunction();

// nestedFunction(); results to an error

// Function and Global Scoped Variables

function myNewFunction2() {
  let nameInside = "Mr. Funciton Scope";
  console.log(globalVar);
}
myNewFunction2();
// console.log(nameInside); results to an error

// Using alert()

// alert() allows us to show a small window at the top of our browser to show infromation to our users. As opposed to a console.log() which only shows the message in the console. This page will wait untile the user dismiss the dialog box.

// alert("Hello World!");
/* 
    Syntax:
        alert("<message string>");
*/

function showSampleAlert() {
  alert("Hello, User!");
}

// showSampleAlert();
// showSampleAlert();

// Show only an alert() for short dialogs/messages to our users.
// Do not overuse alert() because the program/JavaScript has to wait for it to be dismissed before continuing/

// Using prompt()

// prompt() allows us to show a small window at the top of the browser to gather user input. It, much like alert(), it will have the page wait until the user completes/enters their input. The input from the prompt() wil be returned as a String once the user dismisses the window.

// let samplePrompt = prompt("Enter your Name: ");

// console.log("Hello, " + samplePrompLevint);

/* 
    Syntax:
    prompt("<dialogInString>");
*/

// function printWelcomeMessage() {
//   let firstName = prompt("Enter Your First Name: ");
//   let lastName = prompt("Enter Your Last Name Name: ");

//   console.log("Hello, " + firstName + " " + lastName + "!");
//   console.log("Welcome to my page!");
// }

// printWelcomeMessage();

// Funciton Naming Conventions

// Function names should be definitive of the task it will perform. It usually contains a verb.

function getCourses() {
  let courses = ["Science 101", "Math 101", "English 101"];
  console.log(courses);
}

getCourses();

// Avoid pointless and inappropriate function names.

function foo() {
  console.log(25 % 5);
}

foo();

// Name your functions in small caps. Follow camelCase when naming variables and functions/.
